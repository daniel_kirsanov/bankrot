$(function () {
    var cookieName = 'menuToggle';
   	var cookieOptions = {expires: 30, path: '/'};
	var doc = $(document);
	var win = $(window);
	var menu = $('.menu');
	var menuActive = $('.menu-active');
	var menuPassive = $('.menu-passive');
	var menuStatus = 0;

    if ($.cookie(cookieName) == 1) {
        menuStatus = 1;
    } else {
        $.cookie(cookieName, menuStatus, cookieOptions);
    }

	function menuStart () {
		var top = doc.scrollTop();
		if (top > 0) {
			menu.addClass('menu_fixed');
			if (menuStatus == 0) {
				menuPassive.show();
				menuActive.hide();
			} else {
				menuPassive.hide();
				menuActive.show();
				menu.addClass('menu_hide');
			}
		} else {
			menu.removeClass('menu_fixed');
			menuPassive.hide();
			menuActive.hide();
			menu.removeClass('menu_hide');
		}
	}

	menuPassive.click(function () {
		$(this).hide();
		menu.addClass('menu_hide');
		menuActive.show();
		menuStatus = 1;
		$.cookie(cookieName, 1, cookieOptions);
	});

	menuActive.click(function () {
		$(this).hide();
		menu.removeClass('menu_hide');
		menuPassive.show();
		menuStatus = 0;
		$.cookie(cookieName, 0, cookieOptions);
	});

	win.on('scroll', function () {
		menuStart();
	});

	menuStart();
});

$(function () {
	$('.banner').each(function () {
		var banner = $(this),
			positionName = 'position',
			icon = banner.find('.banner__s'),
			iconActive = 'banner__s0',
			face = banner.find('.banner__i'),
			faceActive = 'banner__i0',
			content = banner.find('.banner__t'),
			contentActive = 'banner__t0',
			arrowTop = banner.find('.banner__bt'),
			arrowBot = banner.find('.banner__bb'),
			arrowTopPassive = 'banner__bt0',
			arrowBotPassive = 'banner__bb0',
			start = 0,
			end = icon.length - 1,
			position = banner.data(positionName);

		function bannerInit(p) {
			icon.each(function (i, e) {
				if (i == p) {
					$(e).addClass(iconActive);
				} else {
					$(e).removeClass(iconActive);
				}
			});

			face.each(function (i, e) {
				if (i == p) {
					$(e).addClass(faceActive);
				} else {
					$(e).removeClass(faceActive);
				}
			});

			content.each(function (i, e) {
				if (i == p) {
					$(e).addClass(contentActive);
				} else {
					$(e).removeClass(contentActive);
				}
			});

			if (p == start) {
				arrowTop.addClass(arrowTopPassive);
				arrowBot.removeClass(arrowBotPassive);
			} else if (p == end) {
				arrowTop.removeClass(arrowTopPassive);
				arrowBot.addClass(arrowBotPassive);
			} else {
				arrowTop.removeClass(arrowTopPassive);
				arrowBot.removeClass(arrowBotPassive);
			}
		}

		bannerInit(position);

		arrowTop.click(function () {
			if (position != start) {
				position--;
				bannerInit(position);
			}
		});

		arrowBot.click(function () {
			if (position != end) {
				position++;
				bannerInit(position);
			}
		});

		icon.click(function () {
			var indexIcon = $(this).index() - 1;

			if (indexIcon != position) {
				bannerInit(indexIcon);
				position =  indexIcon;
			}
		});

	});
});
